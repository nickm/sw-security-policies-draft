

This repository is for drafting ideas about generalizing the
network team's
[policy on security bugs](https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/SecurityPolicy)
so that it can be used (on an opt-in basis) by other projects at Tor.

The [policy.md](./policy.md) file has a draft version of the policy, with
`tor`-specific references removed.  If you read the raw markdown (rather than
looking at the rendered HTML), you'll see places where teams can add
themselves and their projects, and describe how they handle issue severities.


The [adapting.md](./adapting.md) file has instructions for how to adapt the
policy for your own projects.


Ignore the [trove](./trove) directory for now. That's an independent idea.


This is a draft; MRs welcome if you think this is a good approach.
