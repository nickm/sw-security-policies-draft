# Adapting the software security policy for your own use

This document explains how to apply `SecurityPolicy.md` to your own
software, and why you might want to.

## What is this policy for?

These policies try to answer the question: "What will you do when
you learn about a security problem?"

These policies help your users know what to expect when there is a
problem in your software, and what to expect when they report
security issues

These policies help packagers and advanced users understand your
pre-release communications about security issues.

These policies also help you, the developer: By having a clear
process for classifying and responding to security issues, you can
spare yourself the stress of making this kind of decision under
pressure every time a new issue comes in.

Finally, these policies are opt-in!  If you do not say that they
apply to your project, they do not.

## Do you want to apply these policies at all?

These policies assume actively maintained software with active users
that have real security needs.  If your software is
experimental-only, or if you aren't making any maintenance
commitment for it, then you probably shouldn't apply these policies
to your project.

Moreover, these policies were written with the assumption that you
are creating software and distributing it to others.  If you're
doing something else (like running a service, keeping a database, or
making software primarily for internal consumption), they might or
might make sense for you.

If you're interested in applying them anyway, let's figure out how!


## What, briefly, do the policies do?

If you apply these policies, you commit to doing the following:

1. Explain how you would like people to report security issues.

2. Explain how you plan to classify incoming security issues,
   and try to follow that plan.

3. Try to keep higher-severity issues confidential before a fix is
   available for users.

4. Try to fix higher-severity issues promptly, and backport those
   fixes.

5. Communicate clearly about how severe issues are.

6. Get TROVE IDs for all security issues.

7. Get CVE IDs for higher-severity issues.

8. Make it clear which releases are supported and which are not.

9. Deviate from the above when doing so is necessary to keep users
   safe.

## How do I apply these policies to my software?

There are a few steps you need to take in order to customize these
policies so that they apply to your use case.  These are listed in
the policy document: look for HTML comments that start with the
string "ADOPTION".

Specifically, you will need to decide:

 - What software does the policy apply to?
 - How should people report sensitive security issues?
 - What are some typical examples of different security severities
   for your project?

Additionally, you should read the whole document, and see whether
anything it says anything that you _don't_ intend to do.


