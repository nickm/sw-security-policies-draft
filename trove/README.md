

Here's the start of a couple of scripts we might use to manage TROVE issues
as a repository rather than a wiki page.









==============================

This is a registry of security vulnerabilities in software made by Tor.

It is based on
https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/TROVE, but
adapted to be more maintainable by multiple teams.

There is an internal database that uses one file per issue, and a build
script that assembles a table of known issues.

To add a new issue, run "bin/new_issue".

To build the page, run "bin/build_page".

