# Tor software security issue response policy

(This document is based on the network team's "security issue
response policy", which was provisionally adopted in March 2020. It
became non-provisional on August 1 2020.)

## Preamble

This document describes how some projects at Tor *plan* to handle
security issues.  It is an attempt to specify our plans
in advance, and commit to them in advance, so that we can speed up our
decision-making, and give users a better idea of what to expect.

This is a *plan* that we will adapt as the circumstances warrant.
The golden rule will be:

> When in doubt, protect users.

Notwithstanding anything in the rest of this document, we should not
follow this policy if doing would be harmful to users.

## Meta-issues

### I am deeply suspicious; How should I read this document?

Please read it as having been written by and for honest people of
good intent. It is not intended to be sufficient on its own to
guarantee good outcomes from people more determined to comply with
its letter than its spirit. Rather, it is intended to reflect a
consensus on the right way to do things, help people of good intent
make the right decisions under pressure, and let users know what to
expect.

### What does this document apply to?

These policies apply to any software whose maintainers have adopted
them.  Currently, this includes:

 - [`tor`](https://gitlab.torproject.org/tpo/core/tor/)

<!-- ADOPTING: Add your project to the list! -->

## The process

### What should I do if I find a security flaw in something Tor makes?

First, try to make sure it's a real security flaw; see section 3
below.

If the flaw is low-severity or already public, you can just open
a public ticket on the
[bugtracker](https://gitlab.torproject.org).  (You can
[request an account](https://gitlab.onionize.space) or
[report a bug anonymously](https://anonticket.onionize.space).)

If you find a security flaw in a Tor product that has adopted these
guidelines, and the flaw is neither low-severity nor already public,
please either open a _confidential_ ticket on the bugtracker, or
use private email to tell the appropriate developers.

The correct email contact is:

<!-- ADOPTING: Insert your team's email contact here if it is
  -- different. Don't use a personal email address. -->

 - ​tor-security@lists.torproject.org

You can either send your email unencrypted, or encrypt it to the
appropriate PGP key:

```
pub   4096R/E135A8B41A7BF184 2017-03-13
      Key fingerprint = 8B90 4624 C5A2 8654 E453  9BC2 E135 A8B4 1A7B F184
uid                          tor-security@lists.torproject.org <tor-security@lists.torproject.org>
uid                          tor-security@lists.torproject.org <tor-security-request@lists.torproject.org>
uid                          tor-security@lists.torproject.org <tor-security-owner@lists.torproject.org>
```

<!-- ADOPTING: Insert your team's email's PGP key here. -->

Please don't rely on twitter direct messages, social media, online
chat, blog comments, postal mail, messages in bottles, or notes
wrapped around bricks: anything that doesn't get sent via the right
mechanism is at risk of being missed, misclassified, misevaluated,
or misfiled.

You might also be interested in our
[HackerOne program](https://hackerone.com/torproject).  Note that it
does not apply to all of our software.

### How will the developers handle security issues?

First, we will assess whether an issue is already publicly disclosed, and we'll
try to classify it as "research," "low-severity," "medium-severity,"
"high-severity", or "critical-severity" according to our [triage
guidelines below](#triage).

This will drive our decision about how to handle the issue:
  - "Research" issues are always handled in public.
  - "Low-severity" issues are generally handled in public.
  - Issues that are already publicly disclosed, or under active
    exploitation[^1] are handled in public.
  - Other issues are generally treated as confidential, if possible.

Next, we will begin tracking the issue:
  * If the issue is public, we'll open a public ticket.
  * If the issue is confidential, we will open a confidential ticket
    to discuss the details, and a empty public ticket
    to track its progress.
  * In all cases, we'll allocate a [TROVE-ID][TROVE] to track our progress
    and identify the issue.

Then we'll work on fixing the issue, with appropriate urgency
depending on its severity.

When we're about to release a fix, we may make an announcement in
advance to let people know about the upcoming security release.
We'll always do this is the issue is of *high* or *critical*
severity.

Finally, we'll release a fix:

  * If the issue is *low* severity, we will fix it in the
    development branch of our code, and backport it our discretion.
  * If the issue is *medium* severity, we will apply the fix soon
    before release, and we'll backport it at our discretion,
    generally to the most recent supported stable release.  We won't
    generally adjust the release schedule for these issues.
  * If the issue is of *high* or *critical* severity, we'll apply
    the fix right before release, and we may produce an unscheduled
    release early, in order to get the issue fixed.  We'll backport
    fixes for these issues to all supported releases.  We'll get
    CVD-IDs for this issue.
  * Additionally, if the issue is of *critical* severity, we'll try
    to coordinate in advance with packagers and affected parties to
    make sure that everybody is able to update as quickly as
    possible.

Here is a summary table:

| Severity   | Confidential? | Release ASAP? | Pre-announce | Backport?     | Coordinated Release? | CVE-ID?      |
| --------   | ------------- | ------------- | ------------ | ---------     | -------------------- | -------      |
| *research* | No            | No            | No           | No            | No                   | Generally No |
| *low*      | No            | No            | No           | Maybe         | No                   | No           |
| *medium*   | Generally yes | No            | Generally no | Latest stable | No                   | Generally no |
| *high*     | Yes           | Yes           | Yes          | All supported | Sometimes            | Yes          |
| *critical* | Yes           | Yes           | Yes          | All supported | Always               | Yes          |

Finally, we'll make our full information about the vulnerability
available:

  * For *low* and *medium* vulnerabilities, we will generally try
    make all previously confidential info public upon release, or
    within a day or two from the release.
  * For *high* and *critical* vulnerabilities, we'll may keep some
    details private for a little while after the release, if we
    judge that doing so will present a real obstacle to attackers.




[1]: "Publicly disclosed issues" include any issue which we believe
     that is being actively exploited to harm users in the wild:
     there is no point keeping a problem secret if attackers already
     know about it.  We'll In these cases, we'll inform people about
     the scope and extent of the attack as we become aware of it,
     even if we don't have a fix yet. In deciding whether to do so,
     we will act so as to maximize user safety.

### How will we <a name="triage"></a>assess the severity of a security issue?

We try to classify security issues as "research," "low-severity,"
"medium-severity," "high-severity", or "critical-severity". We may
also classify an issue as "not a security flaw" or "upstream issue."
Here we discuss the general principles behind these severities, and
give project-by-project examples.

In the abstract:
  * A *research* issue is one that we would like to solve, but
    for which no best or implementable solution is known.
  * An issue is *not a security flaw* in our software if it would
    require our software to provide some security property it was
    never intended to provide.
  * A *low-severity* issue is one where fixing the issue will
    improve security or privacy a little, but the issue's presence
    is not an active threat to users.
  * A *medium-severity* issue presents a real security or privacy problem,
    but one of limited scope (not relevant to many users) or limited
    impact (effect of exploit is comparatively harmless).
  * A *high-severity* issue presents an active threat to users'
    privacy, anonymity, or security.
  * A *critical-severity* issue presents an active and catastrophic
    threat to users' privacy, anonymity, or security.
  * An *upstream* issue is a bug in somebody else's software.
  * All of the severities may be adjusted upwards or downwards
    depending on how many users are affected, how hard the issues
    are to exploit, and how bad the impact of an exploit.

Another way to consider severities is by looking at what advice we
would give users about them:

  * The response to a *research* issue is generally, "That is an
    active area of research for which no applicable solution is
    currently known. Some promising avenues of research are..."
  * The response to an issue that is *not a security flaw* is
    generally "Our software is not intended to protect against that"
    or "So don't do that then".
  * If an issue is *low-severity*, its fix is usually accompanied with
    no specific advice to upgrade.
  * The fix for a *medium-severity* issue is usually accompanied
    with low-urgency advice like "affected users should upgrade when
    they can."
  * The fix for a *high-severity* issue is usually accompanied
    with advice like "all affected users should upgrade as soon as
    possible."
  * The fix for a *critical-severity* issue is accompanied
    with urgent advice like "we STRONGLY advice all users to upgrade
    NOW", and usually requires a coordinated release process to
    minimize the window when users are vulnerable.
  * The fix for an *upstream* issue is generally accompanied with
    advice like, "Everybody should upgrade to the latest version of
    \[UPSTREAM PACKAGE\]", with urgency depending on the severity of
    the issue.

Some specific examples:

Some issues arise because of unanswered **research questions**, not
because of bugs in the Tor software. These include:

 * End-to-end traffic correlation by an adversary who can observe
   both ends of the Tor circuit channel.
 * Profiling attacks by waiting for a long time to become somebody's
   guard.
 * Website- and traffic-type fingerprinting attacks.

<!-- ADOPTING: add more examples here for your software. -->
<!-- ADOPTING: discuss whether any of the issues above are different -->
<!-- for your software. -->

In general, if no best or implementable solution is known for a
given issue, it should be treated as a research problem rather than
a security bug. These issues *matter*, and sometimes matter as
deeply as any high-severity issue, but we shouldn't keep them
private. Instead we will engage the research community for help
solving them.

Here are some things that typically count as **low-severity**
security issues, or not as security issues at all:

 * A program can be made to crash or work incorrectly, but the
   program's user is the only one who can cause this.
 * An attack is possible by a class of attacker that Tor does not
   attempt to defend against. (For example, Tor assumes that the
   attacker does not have administrator access to your computer, has
   not installed a keylogger, does not control a majority of
   directory authorities, cannot make an authenticated connection to
   the control port, and so on.)
 * An attack is possible when the user ignores the advice on our
   download page.
 * When users go to the wrong onion service address, they get the
   wrong onion service.
 * When users ignore clear certificate warnings from the browser,
   they are vulnerable to MITM from an exit node.
 * When users ignore a warning that doing something will make Tor
   less secure, Tor becomes less secure.
 * At significant expense or effort, an attacker can cause a denial
   of service to a relay.
 * Timing side-channel attacks are present, but can only be
   exploited at great difficulty, and only by local users.
 * A bug affects only unsupported versions of our software.
 * Security bugs that only affect unsupported platforms (like
   Irix, Windows 98, Linux 1.3, etc).

<!-- ADOPTING: add more examples here for your software. -->
<!-- ADOPTING: discuss whether any of the issues above are different -->
<!-- for your software. -->

These are **typically** low-severity issues:

 * A defense-in-depth mechanism provides less defense-in-depth than
   it should (with no known corresponding attack enabled). For
   example, if sensitive material remains on the stack or heap
   without getting memwipe'd, but there is no means to exfiltrate
   it, it is typically low-severity.
 * Undefined behavior is invoked, but not in a way that actually
   causes undesirable behavior when interpreted by any compiler we
   support.

<!-- ADOPTING: add more examples here for your software. -->
<!-- ADOPTING: discuss whether any of the issues above are different -->
<!-- for your software. -->


Anything in these category is typically a **medium-severity** issue:

 * Any remote crash or denial-of-service attack that does not affect
   clients or onion services, only relays. (This includes unfreed
   memory and other resource exhaustion attacks that can lead to
   denial-of-service.)
 * Security bugs affecting configurations which almost nobody uses.
 * Timing side-channel attacks that can be observed remotely, but
   only at great difficulty.
 * Security bugs that require local (non privileged) access to your
   computer to exploit.
 * Security bugs that make client path selection different to what
   is documented (for example: a bug that makes related relays more
   likely to be selected in the same path)

<!-- ADOPTING: add more examples here for your software. -->
<!-- ADOPTING: discuss whether any of the issues above are different -->
<!-- for your software. -->

Anything in these categories is typically a **high-severity** issue,
unless it is classified as lower severity because of one of the
definitions above:

 * Any bug that can remotely cause clients to de-anonymize
   themselves.
 * Any remote crash attack against onion services. (This includes
   unfreed memory and other resource exhaustion attacks that can
   lead to denial-of-service.)
 * Any memory-disclosure vulnerability.
 * Any bug that allows impersonation of a relay. (If someone
   accesses a relay's keys, and it's not due to a bug in tor, we
   deal with that through the bad-relays process.)
 * Any bug that lets non-exit relays to see user plaintext.
 * Any privilege escalation from a Tor user to the higher-privileged
   user that started the Tor process. (For example, if Tor is
   started by root and told to drop privileges with the User flag,
   any ability to regain root privileges would be high-severity.)

Anything in these categories is typically a **critical-severity**
issue, unless it is classified as lower severity because of one of
the definitions above:
 * Any remote code-execution vulnerability.

Some bugs affect our software, but are **upstream bugs**, not bugs in Tor
itself: they include bugs in external libraries, like OpenSSL,
Libevent, or zlib; bugs in an operating system's kernel; or bugs in
upstream Firefox affecting TorBrowser. When we become aware of an
"upstream" issue like this, we will coordinate with the upstream
developers to find and deploy an appropriate fix, in accordance with
their own security processes.

Finally, _the above categories are approximation only_; difficulty
of exploitation, degree of impact, rarity of configuration, and
other public factors may increase or decrease the security of an
issue.

### Can I show you my research findings in advance of disclosure? Or will you spoil my conference talk?

We'd rather have a fix today than a fix at the conference; but we'd
rather have a fix that goes live the day of your talk than a fix
that won't be ready till a week later.

Therefore, if you want to coordinate our disclosure to correspond
with yours, we can arrange that, assuming that the interval during
which you want us to keep the issue private (that is, to "embargo"
it) is not longer than 60 days.

We will *not* embargo an issue under any of the following
circumstances:

 * The issue becomes public through other means, or somebody else
   finds it.
 * We have reason to believe that the issue is being exploited
   against real users in the wild.

If one of these situations does occur, we will make an effort to
alert you to the situation, and will coordinate with you to
acknowledge your research and help: but our first priority will be
to our users.

### Who will find out about non-public flaws? When?

The core developers of the affected software component, and the
research director (Roger) will find out immediately. We will work
together with the bug reporter to identify and validate a solution.

We may enlist packagers, researchers and testers as needed to ensure
that our fix is correct, and complete, and doesn't break anything
else.

All other packagers will learn that there is an upcoming release
that will fix a security flaw, and will learn the flaw's general
severity, but won't learn specific information.

(And of course, everybody will learn about the flaw when it becomes
public.)

### Will you attribute bug reports and fixes?

Yes, unless you ask us not to.

### How will you publicize issues and fixes?


All security fixes will appear in the relevant software's changelogs
and release notes.  When appropriate, the changelog and other forms
of announcement will feature a prominent sentence saying who should
update.

Additionally, all high- and critical-severity issues will be
announced via public means that are likely to reach a large number
of user, including: https://blog.torproject.org/, and relevant
"announcement" lists and social media accounts.

We will look into more ways to announce and publicize fixes and
issues.

### Will you get CVEs?

Yes, for all high- and critical-severity issues.

You can find a list of _all_ issues in our own [TROVE] registry,
here on our wiki.

## Secondary contact info

If for some reason the tor-security mailing list doesn't work for
you, or you want to make initial contact in an encrypted way, please
use the PGP keys here to encrypt your description of the issue,
sending it to the appropriate project leads.

If you do not get a response, however, please try the tor-security
mailing list above: do not assume that your email got through!

 * **Tor:**
    * Nick Mathewson <​nickm@torproject.org>

      2133 BC60 0AB1 33E1 D826 D173 FE43 009C 4607 B1FB
 * **Tor browser**
    * Georg Koppen <​gk@torproject.org>

      35CD 74C2 4A9B 15A1 9E1A 81A1 9437 3AA9 4B7C 3223
 * **Research director**
    * Roger Dingledine <​arma@torproject.org>

      F65C E37F 04BA 5B36 0AE6 EE17 C218 5258 19F7 8451

## Acknowledgments

Thanks to everyone who offered helpful suggestions on earlier
drafts, including Arlo Breault, Cass Brewer, David Goulet, George
Kadianakis, Georg Koppen, Kate Krauss, Lunar, and Tom Ritter.

Thanks to everyone over the years who has patiently explained a
security problem that we didn't understand to us until we finally
understood what they were talking about.



[TROVE]: https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/TROVE
